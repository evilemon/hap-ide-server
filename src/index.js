import './lib/regenerator'

import conf from './lib/conf'
import moduler from './lib/modules'

import { start } from './server'

// 加载模块
moduler.init(conf)

// 启动服务器
start(conf, moduler)