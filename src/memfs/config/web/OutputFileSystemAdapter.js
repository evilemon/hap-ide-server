"use strict";

const fs = require("fs");
const path = require("path");
const mkdirp = require("mkdirp");

class OutputFileSystemAdapter {
	constructor() {
		this.mkdirp = function (path) {
      const memFS = process.webpackFileData
      return memFS.mkdirp(...arguments)
    };
		this.mkdir = function (path) {
      const memFS = process.webpackFileData
      return memFS.mkdir(...arguments)
    };
		this.rmdir = function (path) {
      const memFS = process.webpackFileData
      return memFS.rmdir(...arguments)
    };
		this.unlink = function (path) {
      const memFS = process.webpackFileData
      return memFS.unlink(...arguments)
    };
		this.writeFile = function (path) {
      const memFS = process.webpackFileData
			return memFS.writeFile(...arguments)
    };
		this.join = path.join.bind(path);

		// 其它插件中用到的API
    this.readdirSync = function (path) {
      const memFS = process.webpackFileData
      return memFS.readdirSync(...arguments)
    };
    this.readFileSync = function (path) {
      const memFS = process.webpackFileData
      return memFS.readFileSync(...arguments)
    };
    this.statSync = function (path) {
      const memFS = process.webpackFileData
      return memFS.statSync(...arguments)
    };
    this.writeFileSync = function (path) {
      const memFS = process.webpackFileData
      return memFS.writeFileSync(...arguments)
    };
	}
}

module.exports = OutputFileSystemAdapter;
