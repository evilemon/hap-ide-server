/**
 * Copyright (C) 2017, hapjs.org. All rights reserved.
 */

/**
 * @file 为了在低版本node(早于7.6.0)之前的版本上可以跑通koa，注入babel-runtime/regenerator
 */

import ReGen from 'babel-runtime/regenerator'

global.regeneratorRuntime = ReGen
