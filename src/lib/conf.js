/**
 * Copyright (C) 2017, hapjs.org. All rights reserved.
 */

import path from 'path'
import yargs from 'yargs'

// 命令行参数
const argv = yargs.argv

// 端口
const defaultPort = '12306'
const serverPort = process.env.PORT || argv.port || defaultPort
// 路径
const pathToolkit = path.resolve(__dirname, '..', '..')
const pathClientLog = path.join(pathToolkit, 'client.json')
// 加载模块参数
const argvModuleList = argv.loadModules ? argv.loadModules.split(',') : []

export default {
  defaults: {
    serverPort,
    pathToolkit,
    pathClientLog
  },
  options: Object.assign(argv, {
    argvModuleList
  }),
  // 源码Demo项目路径
  dirBase: argv.dirBase ? path.resolve(process.cwd(), argv.dirBase) : path.join(process.cwd(), `dir-base`),
  // 源码保存路径
  dirDest: path.join(process.cwd(), `dir-dest`),
  // RPK保存路径
  dirDist: path.join(process.cwd(), `dir-dist`)
}
