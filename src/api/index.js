/**
 * Copyright (C) 2017, hapjs.org. All rights reserved.
 */

const prefix = '/api'

export default {
  'index': prefix + '/hapBundle/:key',
  'hapBundle': prefix + '/hapBundle/:key/bundle',
  'loadDirectory': prefix + '/loadDirectory',
  'saveDirectory': prefix + '/saveDirectory',
  'hapBuild': prefix + '/hapBuild'
}